<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_once _DIR_PLUGIN_OPTIMAGES . 'vendor/autoload.php';

use Spatie\ImageOptimizer\OptimizerChain;

/**
* Tester si un binaire est présent
*
* @param string $nom
* @return bool
*/
function optimages_tester_commande_existe($nom): bool {
	$methode = (false === stripos(PHP_OS, 'win')) ? 'command -v' : 'which';
	return !((null === shell_exec("$methode $nom")));
}

/**
 * Transformer un tableau de résultats SQL en un tableau d'id_documents organisés selon leur extension
 *
 * En entrée, prend un tableau tel que:
 * [
 *      0 => array [
 *          "id_document" => "2"
 *          "extension" => "png"
 *          "fichier" => "png/_46c3648_c.jpg"
 *      ],
 *      1 => array [
 *          "id_document" => "3"
 *          "extension" => "jpg"
 *          "fichier" => "jpg/_043ridet_20140221_9563.jpg"
 *      ],
 *      2 => array [
 *          "id_document" => "4"
 *          "extension" => "jpg"
 *          "fichier" => "jpg/_043ridet_20140221_95634.jpg"
 *      ],
 * ]
 *
 * En sortie, produit un tableau tel que:
 *
 * [
 *      "jpg" => array [
 *          0 => "3"
 *          1 => "4"
 *      ],
 *      "png" => array [
 *          0 => "2"
 *      ],
 * ]
 *
 * @param array $images
 * @return array
 */
function optimages_retourner_images_par_extension(array $images): array {
	$images_par_extensions = [];

	array_walk($images, function ($item) use (&$images_par_extensions) {
		if (!$images_par_extensions[$item['extension']]) {
			$images_par_extensions[$item['extension']] = [];
		}

		$images_par_extensions[$item['extension']] = [...$images_par_extensions[$item['extension']], $item];
	});

	return $images_par_extensions;
}

/**
 * Optimise les images et met à jour la base de données avec les bonnes informations de poids
 *
 * @param string $extension
 * @param array $images
 * @param bool $sauvegarde
 * @return void
 */
function optimages_optimiser(string $extension, array $images, bool $sauvegarde) {
	include_spip('base/abstract_sql');
	include_spip('inc/config');

	$optimizers_chain = new OptimizerChain();

	$config = lire_config("optimages/$extension") ?? [];

	foreach ($config as $tool => $configs) {
		$optimizer = 'Spatie\\ImageOptimizer\\Optimizers\\' . ucfirst($tool);

		if (!empty(lire_config("optimages/$extension/$tool/options"))) {
			$params = preg_split('/,( )?/', lire_config("optimages/$extension/$tool/options"));
			$optimizers_chain->addOptimizer(new $optimizer($params));
		} else {
			$optimizers_chain->addOptimizer(new $optimizer());
		}
	}

	foreach ($images as $image) {
		if ($sauvegarde) {
			copy(_DIR_IMG . $image['fichier'], _DIR_IMG . $image['fichier'] . '.back');
		}

		$optimizers_chain->optimize(_DIR_IMG . $image['fichier']);

		$poids = filesize(_DIR_IMG . $image['fichier']);
		sql_updateq('spip_documents', ['taille' => $poids], 'id_document = ' . intval($image['id_document']));
	}
}

/**
 * Déclenche l'optimisation des images si celle-ci est configuré pour les extensions présentes
 *
 * @param array $docs
 * @return void
 */
function optimages_dispatcher(array $docs): void {
	include_spip('inc/config');
	include_spip('inc/optimages');

	$images_par_extension = optimages_retourner_images_par_extension($docs);

	$sauvegarde = (lire_config('optimages/sauvegarde') ?? '') === 'on';

	foreach ($images_par_extension as $extension => $images) {
		$config = lire_config("optimages/$extension") ?? [];

		$inactif = true;

		foreach ($config as $tool => $configs) {
			$activer = lire_config("optimages/$extension/$tool/activer") ?? '';

			$inactif = !(($activer === 'on'));
		}

		if (!$inactif) {
			optimages_optimiser($extension, $images, $sauvegarde);
		}
	}
}

/**
 * Identifie les images à traiter et appelle le dispatcher si besoin
 *
 * @param array $ids
 * @return void
 */
function optimages_traiter(array $ids): void {
	include_spip('base/abstract_sql');
	include_spip('inc/config');

	$docs = sql_allfetsel(
		[
			'id_document',
			'extension',
			'fichier',
		],
		'spip_documents',
		[
			sql_in('id_document', $ids),
			sql_in('extension', ['jpg', 'png', 'gif']),
		]
	);

	if (!empty($docs)) {
		optimages_dispatcher($docs);
	}
}
