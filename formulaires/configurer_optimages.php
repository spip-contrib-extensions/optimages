<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Définition des saisies pour générer le formulaire de configuration
 *
 * @return array
 */
function formulaires_configurer_optimages_saisies_dist(): array {
	$saisies = [
		[
			'saisie' => 'case',
			'options' => [
				'nom' => 'sauvegarde',
				'label_case' => '<:optimages:configurer_sauvegarde_label_case:>',
				'conteneur_class' => 'pleine_largeur'
			]
		],
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'optimizers',
				'label' => '<:optimages:configurer_optimizers_label:>',
			],
			'saisies' => [
				// JPEG
				[
					'saisie' => 'fieldset',
					'options' => [
						'nom' => 'jpegoptim',
						'label' => '<:optimages:configurer_jpegoptim_label:>',
					],
					'saisies' => [
						[
							'saisie' => 'case',
							'options' => [
								'nom' => 'jpg/jpegoptim/activer',
								'label_case' => '<:optimages:configurer_jpegoptim_activer_label_case:>'
							]
						],
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'jpg/jpegoptim/options',
								'label' => '<:optimages:configurer_jpegoptim_options_label:>',
								'explication' => '<:optimages:configurer_jpegoptim_options_explication:>',
								'afficher_si' => '@jpg/jpegoptim/activer@=="on"',
								'defaut' => '\'-m85\', \'--strip-all\', \'--all-progressive\'',
							]
						],
					]
				],
				// PNG
				[
					'saisie' => 'fieldset',
					'options' => [
						'nom' => 'png',
						'label' => '<:optimages:configurer_png_label:>',
						'attention' => '<:optimages:configurer_png_attention:>',
						'explication' => '<:optimages:configurer_png_explication:>',

					],
					'saisies' => [
						[
							'saisie' => 'case',
							'options' => [
								'nom' => 'png/pngquant/activer',
								'label_case' => '<:optimages:configurer_pngquant_activer_label_case:>'
							]
						],
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'png/pngquant/options',
								'label' => '<:optimages:configurer_pngquant_options_label:>',
								'explication' => '<:optimages:configurer_pngquant_options_explication:>',
								'afficher_si' => '@png/pngquant/activer@=="on"',
								'defaut' => '\'--quality=85\', \'--force\', \'--skip-if-larger\'',
							]
						],
						[
							'saisie' => 'case',
							'options' => [
								'nom' => 'png/optipng/activer',
								'label_case' => '<:optimages:configurer_optipng_activer_label_case:>'
							]
						],
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'png/optipng/options',
								'label' => '<:optimages:configurer_optipng_options_label:>',
								'explication' => '<:optimages:configurer_optipng_options_explication:>',
								'afficher_si' => '@png/optipng/activer@=="on"',
								'defaut' => '\'-i0\', \'-o2\'',
							]
						],
					]
				],
				// GIF
				[
					'saisie' => 'fieldset',
					'options' => [
						'nom' => 'gifsicle',
						'label' => '<:optimages:configurer_gifsicle_label:>',
						'attention' => '<:optimages:configurer_gifsicle_attention:>',
					],
					'saisies' => [
						[
							'saisie' => 'case',
							'options' => [
								'nom' => 'gif/gifsicle/activer',
								'label_case' => '<:optimages:configurer_gifsicle_activer_label_case:>'
							]
						],
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'gif/gifsicle/options',
								'label' => '<:optimages:configurer_gifsicle_options_label:>',
								'explication' => '<:optimages:configurer_gifsicle_options_explication:>',
								'afficher_si' => '@gif/gifsicle/activer@=="on"',
								'defaut' => '\'-b\', \'-O3\'',
							]
						],
					]
				],
			]
		]
	];

	include_spip('inc/optimages');
	// Affichage conditionné à la présence de jpegoptim
	if (optimages_tester_commande_existe('jpegoptim')) {
		$saisies[1]['saisies'][0]['options']['attention'] = '<:optimages:configurer_jpegoptim_attention:>';
	}
	else {
		$saisies[1]['saisies'][0]['options']['attention'] = '<:optimages:configurer_jpegoptim_attention_absent:>';
		$saisies[1]['saisies'][0]['saisies'][0]['options']['disable'] = 'true';
	}

	// Affichage conditionné à la présence de pngquant
	if (optimages_tester_commande_existe('pngquant')) {
		$saisies[1]['saisies'][1]['saisies'][0]['options']['attention'] = '<:optimages:configurer_pngquant_attention:>';
	}
	else {
		$saisies[1]['saisies'][1]['saisies'][0]['options']['attention'] = '<:optimages:configurer_pngquant_attention_absent:>';
		$saisies[1]['saisies'][1]['saisies'][0]['options']['disable'] = 'true';
	}

	// Affichage conditionné à la présence de optipng
	if (optimages_tester_commande_existe('optipng')) {
		$saisies[1]['saisies'][1]['saisies'][2]['options']['attention'] = '<:optimages:configurer_optipng_attention:>';
	}
	else {
		$saisies[1]['saisies'][1]['saisies'][2]['options']['attention'] = '<:optimages:configurer_optipng_attention_absent:>';
		$saisies[1]['saisies'][1]['saisies'][2]['options']['disable'] = 'true';
	}

	// Affichage conditionné à la présence de gifsicle
	if (optimages_tester_commande_existe('gifsicle')) {
		$saisies[1]['saisies'][2]['options']['attention'] = '<:optimages:configurer_gifsicle_attention:>';
	}
	else {
		$saisies[1]['saisies'][2]['options']['attention'] = '<:optimages:configurer_gifsicle_attention_absent:>';
		$saisies[1]['saisies'][2]['saisies'][0]['options']['disable'] = 'true';
	}

	return $saisies;
}
