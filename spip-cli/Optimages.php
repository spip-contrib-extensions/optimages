<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Optimages extends Command {
	private const _EXTENSIONS = ['jpg', 'png', 'gif'];

	protected function configure(): void {
		$this
			->setName('optimages:optimiser')
			->setDescription('Optimise les images d\'un site SPIP.')
			->addOption('extension', null, InputOption::VALUE_REQUIRED, 'Extension des images à traiter')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output): int {
		include_spip('base/abstract_sql');

		$extension = $input->getOption('extension') ?? '';

		if ($extension) {
			if (in_array($extension, self::_EXTENSIONS)) {
				$where = 'extension = ' . sql_quote($extension);
			} else {
				$output->writeln('Extension de fichier non prise en charge.');
				return static::FAILURE;
			}
		} else {
			$where = sql_in('extension', self::_EXTENSIONS);
		}

		$docs = sql_allfetsel(
			[
				'id_document',
				'extension',
				'fichier',
			],
			'spip_documents',
			$where
		);

		$before = sql_fetsel(
			[
				'COUNT(id_document) AS count',
				'SUM(taille) AS volume',
			],
			'spip_documents',
			$where
		);

		$output->writeln($before['count'] . ' images à traiter pour un volume de ' . $before['volume'] . ' octets.');


		if (!empty($docs)) {
			include_spip('inc/optimages');
			optimages_dispatcher($docs);

			$after = sql_fetsel(
				[
					'COUNT(id_document) AS count',
					'SUM(taille) AS volume',
				],
				'spip_documents',
				$where
			);

			$gain = ($before['volume'] - $after['volume']) * 100 / $before['volume'];

			$output->writeln($after['count'] . ' images traitées pour un volume de ' . $after['volume'] . ' octets soit un gain de ' . round($gain, 2) . ' %.');
		} else {
			$output->writeln('Aucune image à traiter.');
		}

		return static::SUCCESS;
	}
}
