# Images optimisées

Images optimisées se propose de réduire la taille des images lors de leur téléversement.

## Dépendances

Le plugin Images optimisées s'appuie sur le [package _image-optimizer_ de Spatie](https://github.com/spatie/image-optimizer) qui fait appel lui-même à différents utilitaires de traitement des images :

- jpegoptim
- optipng
- pngquant
- svgo
- gifsicle
- cwebp

En l'état, le plugin ne gère que les images jpg, png et gif.

### Installation sous Windows

Les binaires sont à mettre dans le dossier System32 de Windows (testé avec Laragon 5) :
- jpegoptim : https://github.com/XhmikosR/jpegoptim-windows/releases
- optipng : http://optipng.sourceforge.net/
- pngquant : https://pngquant.org/ (binary for Windows)
- gifsicle : https://eternallybored.org/misc/gifsicle/
- cwebp : https://developers.google.com/speed/webp/download (seulement bin/cwebp)

## Fonctionnement

Si l'on s'était initialement inspiré du plugin GIS (qui associe un point GIS lors de l'upload d'une image si elle possède des données EXIF) et s'insérant via le pipeline *post_edition* lors de l'ajout d'un document, nous avons revu le fonctionnement du plugin qui s'insère désormais via le pipeline *formulaires_traiter* après BigUp, et qui agit donc dès le téléversement d'images.

Pour le reste, rien ne change :
- on fait une copie de l'image que l'on suffixe avec ".back" si le plugin est configuré de la sorte ;
- on compresse l'image ;
- on met à jour le champ taille du document dans la table spip_documents.

## Une commande SPIP-Cli

Le plugin Optimages propose désormais une commande SPIP-Cli: `optimages:optimiser`.
Il est possible de ne procéder à l'optimisation que d'un seul format d'images avec l'option `--extension`, par exemple :

```
spip optimages:optimiser --extension=jpg
```
