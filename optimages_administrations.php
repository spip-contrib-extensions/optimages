<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function optimages_upgrade($nom_meta_base_version, $version_cible): void {
	$maj = [];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}
function optimages_vider_tables($nom_meta_base_version): void {
	effacer_config('optimages');
	effacer_meta($nom_meta_base_version);
}
