# CHANGELOG

## 0.1.6 - 7 juin 2024

- docs: on débute un CHANGELOG.md
- chore: mise à jour de la librairie spatie/image-optimizer vers sa version 1.7.5
- fix: les afficher_si du formulaire de configuration ne s'appuyaient plus sur les bons noms de champs
