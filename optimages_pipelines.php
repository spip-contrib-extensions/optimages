<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Branchement sur traiter
 *
 * Sur les formulaires d'ajout de document(s) :
 *  - editer_document
 *  - joindre_document
 * récupérer les identifiants des documents et les transmettre à optimiser_traiter
 * qui se charge ou non d'optimiser si ce sont des images au format jpg, png ou gif
 *
 * @param array $flux
 * @return array
 */
function optimages_formulaire_traiter(array $flux): array {
	if (
		($form = $flux['args']['form'])
		&& in_array($form, ['editer_document', 'joindre_document'])
	) {
		$ids = [];

		switch ($form) {
			case 'editer_document':
				// Limiter aux seuls cas où l'on change le fichier
				if (
					_request('joindre_upload')
					|| _request('joindre_ftp')
					|| _request('copier_local')
				) {
					$ids = [...$ids, $flux['args']['args'][0]];
				}
				break;
			case 'joindre_document':
				$ids = [...$ids, ...$flux['data']['ids']];
				break;
		}

		if (!empty($ids)) {
			include_spip('inc/optimages');
			optimages_traiter($ids);
		}
	}

	return $flux;
}

/**
 * Branchement sur post_edition
 *
 * À la suppression d'un document, il faut veiller à supprimer son éventuelle sauvegarde.
 *
 * @param array $flux
 * @return array
 */
function optimages_post_edition(array $flux): array {
	if (isset($flux['args']['operation']) && ($flux['args']['operation'] == 'supprimer_document')) {
		$document = $flux['args']['document'];
		if (in_array($document['extension'], ['jpg', 'png', 'gif'])) {
			$fichier = $flux['args']['document']['fichier'];
			$sauvegarde = $fichier . '.back';

			if (file_exists(_DIR_IMG . $sauvegarde)) {
				unlink(_DIR_IMG . $sauvegarde);
			}
		}
	}

	return $flux;
}
